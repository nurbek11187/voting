const { expect } = require("chai")
const { ethers } = require("hardhat");
require("@nomiclabs/hardhat-web3");

const { deployContract } = require("ethereum-waffle");


describe("Voting", function(){
    let owner
    let other
    let acc4
    beforeEach(async function(){

        [owner, other, acc3, acc4] = await ethers.getSigners()
        const Voting = await ethers.getContractFactory("Voting");
        voting = await Voting.deploy()
        await voting.deployed()
        console.log(voting.address)
    })
    it("should be deployed", async function(){
        expect(voting.address).to.be.properAddress
    })

    it("start voting", async function(){
        const startTransaction = await voting.createVoting([acc4.address, acc3.address], ["txt", "txt1"])
        await startTransaction.wait()
       
    })


    it("list of candidates", async function(){
        const listOfCandidates = await voting.getVotingInformation(1)
    })
    it("add candidate", async function(){
        const listOfCandidates = await voting.addCandidate(1,acc4.address, "vote")
    })
    it("add candidate2", async function(){
        const listOfCandidates = await voting.addCandidate(1,acc4.address, "vote2")
    })

    it("vote for candidate", async function(){
        let value1 = ethers.utils.parseEther("0.01")
        const voteToCandidate = await voting.vote(0,0, {value:value1})
    })
    it('get voters array', async function(){
        const getVotersArray = await voting.getVotersArray(0)
    })
    it('get voting information', async function(){
        const getVotersArray = await voting.getVotingInformation(0)
    })


})